const Vue = require('vue');
const VueRouter = require('vue-router');

Vue.use(VueRouter);

routes: [
	{	
		path: '/',
		name: 'index',
		component: (resolve) => require(['./components/home/Home.js'], resolve)
	},
	{
		path: '/auth',
		name: 'auth',
		component: (resolve) => reqire(['./components/auth/Auth.js'], resolve)
	},

	{
		path: '/budgets/:id',
		name: 'budgets',
		component: (resolve) => require(['./components/budget/Budget.js'], resolve)
	}
]


const router = new VueRouter({
	root: '/'
});

const app = new Vue({
	router
}).$mount('#app');