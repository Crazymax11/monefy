const express = require('express');
const router = express.Router();
const main = require('./index/main.js');
const auth = require('./index/auth.js');

router.use('/auth', auth);
router.use('/', main);

const api = require('./api/v1/index.js');

router.use('/api', api);

router.use('*', (req, res, next) => {
	res.render('index', {initialData} = res);
});

module.exports = router;