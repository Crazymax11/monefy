const express = require('express');
const router = express.Router();
const budgetsModel = new (require('../../../models/budgets.js'))();

// creates budget
router.post('/', (req, res, next) => {
	budgets.create({name} = req.body)
		.then( result => res.send(result))
		.catch( err => res.send(err));
});

router.get('/', (req, res, next) => {
	budgets.list()
		.then( result => res.send(result))
		.catch( err => res.send(err));
});


router.get('/:id', (req, res, next) => {
	budgets.show({id} = {req.params.id})
		.then( result => res.send(result))
		.catch( err => res.send(err));
});

module.exports = router;