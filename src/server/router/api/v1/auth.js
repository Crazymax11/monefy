const express = require('express');
const router = express.Router();
const usersModel = new (require('../../../models/users.js'))();

// creates user
router.post('/', (req, res, next) => {
	users.createUser({login, pass, name} = req.body)
		.then( result => res.send(result))
		.catch( err => {
			res.status(500).send(err);
		});
});


// get info about user
router.get('/', (req, res, next) => {
	console.log('get');
	usersModel.getUsers()
		.then( users => res.send(users))
		.catch( err => res.send('err'+err))
});



// HACK
router.get('/login', (req, res, next) => {
	users.doesLoginPassExist({login, pass} = req.body)
		.then( result => {
			if (!result) {
				res.status(400).send('wrong');
			} else {
				res.send('ok');
			}
		})
});


// patch
router.patch('/', (req, res, next) => {
	next();
})

module.exports = router;