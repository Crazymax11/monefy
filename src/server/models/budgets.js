const BaseModel = require('./base.js');

class BudgetsModel extends BaseModel{
	constructor(){
		super(arguments);
	}


	_getCollection() {
		console.log(this.db);
		if (!this._collection) {
			this._collection = this.db.collection(this._collectionName);
		}

		console.log(this._collection);

		return this._collection;

	}


	list() {
		return new Promise((resolve, reject) => {
			this._getCollection().find({}).toArray((err, budgets) => {
				if (err) {
					return reject(err);
				}
				return resolve(budgets);
			})	
		})
	}

	show({id} = budget) {
		return new Promise((resolve, reject) => {
			this._getCollection().find({id}).toArray((err, users) => {
				if (err) {
					return reject(err);
				}

				return resolve(users);
			})
		});
	}

	create({name} = budget) {
		return new Promise((resolve, reject) => {
			if (!name) {
				return reject(new Error('name not provided'));
			}	

			this._getCollection().insert({name})
		})	
	}
}

UsersModel.prototype._collectionName = 'budgets';

module.exports = UsersModel;