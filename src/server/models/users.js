const BaseModel = require('./base.js');

class UsersModel extends BaseModel{
	constructor(){
		super(arguments);
	}


	_getCollection() {
		console.log(this.db);
		if (!this._collection) {
			this._collection = this.db.collection(this._collectionName);
		}

		console.log(this._collection);

		return this._collection;

	}


	getUsers() {
		return new Promise((resolve, reject) => {
			this._getCollection().find({}).toArray((err, users) => {
				if (err) {
					return reject(err);
				}
				return resolve(users);
			})	
		})
	}

	getUser({name, login} = user) {
		return new Promise((resolve, reject) => {
			this._getCollection().find({name, login}).toArray((err, users) => {
				if (err) {
					return reject(err);
				}

				return resolve(users);
			})
		});
	}

	createUser({name, login, password} = user) {
		return new Promise((resolve, reject) => {
			if (!name || !login || !password) {
				return reject(new Error('name, login or pwd not provided'));
			}	

			this._getCollection().insert({name, login, password})
		})	
	}
}

UsersModel.prototype._collectionName = 'users';

module.exports = UsersModel;