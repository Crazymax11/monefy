const App = require('./App.js');
const router = require('./router/Router.js');

new App({
	router,
	port: 3000,
	templatesPath: process.cwd() + '/templates'
});