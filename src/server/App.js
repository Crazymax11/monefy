const express = require('express');

class App{
	constructor({
		port = 3000,
		router,
		templatesPath
	}){		
		const app = express({
			views: templatesPath
		});
		app.set('views', templatesPath);
		app.set('view engine', 'pug');	
		app.engine('pug', require('pug').__express);
		app.use('/static', express.static('public'));
		app.use('/', router);


		app.listen(port);

		this._app = app;
	}
}

module.exports = App;