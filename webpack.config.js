const webpack = require('webpack');

module.exports = {
	entry: './src/webapp/index.js',
	module: {
        loaders: [
            {
                test: /\.scss$/,
                loaders: ['style', 'css', 'sass']
            },
            {
                test: /\.pug$/,
                loader: 'html!jade-html'
            },
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /node_modules|vue\/dist|vue-hot-reload-api|vue-loader/
            }
        ]
    },
	output: {
		filename: 'bundle.js',
		path: __dirname + '/public/js'
	},
	resolve: {
		root: [
            process.cwd(),
            process.cwd()+'/src/webapp',
		],
         alias: {
            'vue$': 'vue/dist/vue.common.js'
          }
	}
}